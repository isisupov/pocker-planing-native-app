import React from 'react';
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from 'react-router-dom';
import 'rsuite/dist/rsuite.min.css';
import './App.css';
import LoginComponent from '../route-components/login/login.component';
import GameComponent from "../route-components/game/game.component";
import {RouteComponentProps} from "react-router";

export default function App() {
  return (
    <div className='poker-planing'>
      <Router>
        <Switch>
          <Route path="/login" render={(props: RouteComponentProps) => <LoginComponent {...props} />} />
          <Route path="/game" component={GameComponent} />
          <Redirect to="/login" from="/" />
        </Switch>
      </Router>
    </div>
  );
}
