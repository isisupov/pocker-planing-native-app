import React from "react";
import {RouteComponentProps} from "react-router";
import {Button} from "rsuite";
import './game.scss';
import {Player} from "../../models/player.model";
import {io} from "socket.io-client";
import {Socket} from "socket.io-client/build/esm/socket";
import {SocketEvents} from "../../models/socket.events";

interface GameState {
  currentPlayer: Player;
}

export default class GameComponent extends React.Component<RouteComponentProps<any, any, {playerName: string}>, GameState> {
  private static readonly SOCKET_STREAM_NAME: string = "nc.poker.planing.socket";
  private socket: Socket;
  private static readonly CARD_NUMBERS: Array<number> = [1, 2, 3, 4, 5, 7, 10, 15, 18, 20, 40, 70, 100];
  private players: Array<Player> = [
    {id: '1', name: 'Ilya'},
    {id: '2', name: 'Ilya'},
    {id: '3', name: 'Ilya'},
    {id: '4', name: 'Ilya'},
    {id: '5', name: 'Ilya'}
  ];

  constructor(props: RouteComponentProps<any, any, {playerName: string}>) {
    super(props);
    this.state = {
      currentPlayer: {
        id: '',
        name: ''
      }
    };
    this.socket = io("http://localhost:8080");
    this.subscribeOnSocketEvents();
    this.createPlayer();
  }

  private subscribeOnSocketEvents(): void {
    this.socket.on(GameComponent.SOCKET_STREAM_NAME, (payload: {eventName: SocketEvents, payload: unknown}) => {
      console.log("SOCKET EVENT: " + JSON.stringify(payload));
      switch (payload.eventName) {
        case SocketEvents.CREATE_PLAYER_SUCCESS: {
          this.createPlayerCallback(payload.payload as Player);
          break;
        }
      }
    });
  }

  private createPlayerCallback(player: Player): void {
    this.setState({
      currentPlayer: player
    });
  }

  private createPlayer(): void {
    this.socket.emit(GameComponent.SOCKET_STREAM_NAME, {eventName: SocketEvents.CREATE_PLAYER, payload: this.props.history.location.state.playerName});
  }

  onLogoutButtonClick(): void {
    this.socket.emit(GameComponent.SOCKET_STREAM_NAME, {name: 123});
    this.socket.disconnect();
    this.props.history.push('/');
  }

  render(): React.ReactNode {
    return (
      <div className='poker-game'>
        <div className='poker-game__header'>
          <div className='poker-game__header-player'>
            { this.state.currentPlayer.name }
          </div>
          <div className='poker-game__header-counter'>
            Players: 32
          </div>
          <Button onClick={this.onLogoutButtonClick.bind(this)}>Logout</Button>
        </div>
        <div className='poker-game__main'>
          {
            this.players.map((player: Player) => {
              return (
                <div key={player.id} className='poker-game__main-player'>
                  <div className='poker-game__main-player-card'>

                  </div>
                  { player.name }
                </div>
              );
            })
          }
        </div>
        <div className='poker-game__cards'>
          {
            GameComponent.CARD_NUMBERS.map((cardNumber: number, index: number) => {
              return (
                <div key={index} className='poker-game__cards-item'>
                  {cardNumber}
                </div>
              );
            })
          }
        </div>
      </div>
    );
  }
}
