import React from 'react';
import {Input, InputGroup} from 'rsuite';
import './login.scss';
import {RouteComponentProps} from "react-router";

export default class LoginComponent extends React.Component<RouteComponentProps> {
  readonly dnsUrl = "http://localhost:8080";

  onCreateNewButtonHandle(): void {
    fetch(`${this.dnsUrl}/game`, {method: 'POST'})
      .then((response) => {
        return response.text();
      })
      .then((response) => {
        console.log(response);
        this.props.history.push('/game', {playerName: "Ivanich"});
      })
  }

  onJoinButtonHandle(): void {
    fetch(`${this.dnsUrl}/game/132`)
      .then((response) => {
        return response.json();
      })
      .catch(() => console.log("There is no this game id"))
      .then((response) => {
        console.log(response);
      })
  }

  render(): React.ReactNode {
    return (
      <div className="login-component">
        <div className="login-component__label">
          Join existing
        </div>
        <div className="login-component__form">
          <InputGroup className="login-component__form-group">
            <Input placeholder="Pocker game id" className="login-component__form-control" type='text'/>
            <Input placeholder="Your name" className="login-component__form-control" type='text'/>
            <InputGroup.Button onClick={this.onJoinButtonHandle.bind(this)}>Join</InputGroup.Button>
          </InputGroup>
        </div>
        <div className="login-component__label">
          or start new game
        </div>
        <div className="login-component__form">
          <InputGroup className="login-component__form-group">
            <Input placeholder="Your name" className="login-component__form-control" type='text'/>
            <InputGroup.Button onClick={this.onCreateNewButtonHandle.bind(this)}>Create</InputGroup.Button>
          </InputGroup>
        </div>
      </div>
    );
  }
}
